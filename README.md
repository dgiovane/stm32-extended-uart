# Note
This is a copy of the code by mikucukyilmaz https://github.com/mikucukyilmaz/STM32-Extended-UART. It has been modified for the F3 series, moreover timeout features are removed, only character match interrupt is left.
If you need the actual README take a look at the original repository
