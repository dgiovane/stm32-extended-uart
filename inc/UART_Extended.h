#ifndef __UART_Extended_H
#define __UART_Extended_H

#ifdef __cplusplus
 extern "C" {
#endif

#include "main.h"

#define __UARTEX_ENABLE_CM_IT(__HANDLE__)    ((__HANDLE__)->Instance->CR1 |=  USART_CR1_CMIE)
#define __UARTEX_DISABLE_CM_IT(__HANDLE__)   ((__HANDLE__)->Instance->CR1 &= ~USART_CR1_CMIE)

#define __UARTEX_GET_CM_IT(__HANDLE__)       ((((__HANDLE__)->Instance->ISR & (USART_ISR_CMF)) != RESET) ? SET : RESET)
#define __UARTEX_CLEAR_CM_FLAG(__HANDLE__)   ((__HANDLE__)->Instance->ICR = (uint32_t)(USART_ICR_CMCF))

void UartEx_EnableCM(UART_HandleTypeDef* _uartHandle, uint8_t _character);
void UartEx_DisableCM(UART_HandleTypeDef* _uartHandle);

HAL_StatusTypeDef UartEx_CM_IRQHandler(UART_HandleTypeDef* _uartHandle);

#ifdef __cplusplus
}
#endif

#endif /* __UART_Extended_H */
