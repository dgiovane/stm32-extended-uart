#include "UART_Extended.h"

/**
  * @brief Enable UART Receiver Character Match Feature
  * @param huart pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  * @param Character which generate interrupt when detected.
  * @retval None
  */
void UartEx_EnableCM(UART_HandleTypeDef* _uartHandle, uint8_t _character)
{  
  __HAL_UART_DISABLE(_uartHandle);
	_uartHandle->Instance->CR2 |= ((_character << USART_CR2_ADD_Pos ) & USART_CR2_ADD_Msk);
  __UARTEX_CLEAR_CM_FLAG(_uartHandle);
  __UARTEX_ENABLE_CM_IT(_uartHandle);
  __HAL_UART_ENABLE(_uartHandle);
}

/**
  * @brief Disable UART Receiver Character Match Feature
  * @param huart pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  * @param Character which generate interrupt when detected.
  * @retval None
  */
void UartEx_DisableCM(UART_HandleTypeDef* _uartHandle)
{  
  __HAL_UART_DISABLE(_uartHandle);
  __UARTEX_DISABLE_CM_IT(_uartHandle);
  __HAL_UART_ENABLE(_uartHandle);
}


/**
  * @brief Handle UART Character Match Interrupt Request
  * @param huart pointer to a UART_HandleTypeDef structure that contains
  *                the configuration information for the specified UART module.
  * @retval HAL Status
  */
HAL_StatusTypeDef UartEx_CM_IRQHandler(UART_HandleTypeDef* _uartHandle)
{
	if(__UARTEX_GET_CM_IT(_uartHandle))
	{
		__UARTEX_CLEAR_CM_FLAG(_uartHandle);
		return HAL_OK;
	}
	return HAL_ERROR;
}
